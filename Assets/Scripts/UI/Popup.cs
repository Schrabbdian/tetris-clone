﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Tetris
{
    /// <summary>
    /// A UI Text Popup that disappears automatically.
    /// </summary>
    public class Popup : MonoBehaviour
    {
        [Header("Component References")]
        [SerializeField] private TextMeshProUGUI textUI;
        [SerializeField] private Animator animator;

        public void SetText(string text)
        {
            textUI.text = text;
        }

        public void SetTextColor(Color color)
        {
            textUI.color = color;
        }

        public void DestroyPopup()
        {
            Destroy(gameObject);
        }
    }
}
