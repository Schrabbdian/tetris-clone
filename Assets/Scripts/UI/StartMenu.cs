﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// The Start Menu of the Game.
    /// </summary>
    public class StartMenu : FadeOnActive
    {
        /// <summary>
        /// Resets the Grid and Score, destroys all active Tetrominos and spawns a new initial Tetromino.
        /// </summary>
        public void StartNewGame()
        {
            // Destroy unplaced Tetrominos
            var tetrominos = GameObject.FindGameObjectsWithTag("Tetromino");
            foreach (GameObject tetromino in tetrominos)
            {
                Destroy(tetromino);
            }

            Grid.Instance.Clear();
            GameManager.Instance.ResetScore();
            Spawner.Instance.Invoke("CreateNewTetromino", 0.5f);

            FadeOut();
            GameManager.Instance.IsPaused = false;
        }

        /// <summary>
        /// Exits the game.
        /// </summary>
        public void ExitGame()
        {
            Debug.Log("Quitting game...");
            Application.Quit();
        }
    }
}
