﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// The Pause Menu of the Game.
    /// </summary>
    public class PauseMenu : StartMenu
    {
        public void Continue()
        {
            GameManager.Instance.IsPaused = false;
            FadeOut();
        }
    }
}
