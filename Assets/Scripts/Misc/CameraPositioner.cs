﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    public class CameraPositioner : MonoBehaviour
    {
        [Header("Camera Points")]
        [SerializeField] private Transform CameraPointGame;
        [SerializeField] private Transform CameraPointMenu;

        private void Update()
        {
            // Let Camera Lerp to Current focus point
            Transform target = GameManager.Instance.IsPaused ? CameraPointMenu : CameraPointGame;

            transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, 8.0f * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, target.position, 8.0f * Time.deltaTime);
        }
    }
}
