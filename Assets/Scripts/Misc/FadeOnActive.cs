﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Tetris
{
    /// <summary>
    /// A base class for fadeable UI Elements
    /// </summary>
    public class FadeOnActive : MonoBehaviour
    {
        private Coroutine fadeRoutine = null;
        
        [SerializeField] private CanvasGroup canvasGroup;

        /// <summary>
        /// Fade the canvasGroup's Opactity gradually towards the specified target.
        /// </summary>
        /// <param name="targetOpacity"></param>
        /// <returns></returns>
        private IEnumerator FadeRoutine(float targetOpacity)
        {
            targetOpacity = Mathf.Clamp01(targetOpacity);

            while(Mathf.Abs(canvasGroup.alpha - targetOpacity) > 0.05f)
            {
                canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, targetOpacity, Time.deltaTime * 8.0f);
                yield return null;
            }

            canvasGroup.alpha = targetOpacity;
            if (canvasGroup.alpha <= 0.0f) { gameObject.SetActive(false); }
        }

        public void FadeIn()
        {
            if (canvasGroup == null) return;
            if (fadeRoutine != null) { StopCoroutine(fadeRoutine); }

            // Start Fading in routine
            gameObject.SetActive(true);
            fadeRoutine = StartCoroutine(FadeRoutine(1.0f));
        }

        public void FadeOut()
        {
            if (canvasGroup == null) return;
            if (fadeRoutine != null) { StopCoroutine(fadeRoutine); }

            // Start Fading in routine
            fadeRoutine = StartCoroutine(FadeRoutine(0.0f));
        }
    }
}
