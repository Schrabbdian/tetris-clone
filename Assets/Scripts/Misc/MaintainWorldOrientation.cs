﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// This Script can be placed on a child Transform to make it maintain zero rotation around the Z axis in world space.
    /// </summary>
    public class MaintainWorldOrientation : MonoBehaviour
    {
        void Update()
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
        }
    }
}
