﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// This Script controls the behaviour of a Tetris block, including user input, falling and ensuring it is only moved to valid positions.
    /// </summary>
    public class Tetromino : MonoBehaviour
    {
        [Header("Movement Attributes")]
        public float FallCooldown = 2;

        [Header("Input")]
        [Tooltip("Interval with which the left button should trigger when held.")] public float LeftButtonInterval;
        [Tooltip("Interval with which the right button should trigger when held.")] public float RightButtonInterval;
        [Tooltip("Interval with which the up button should trigger when held.")] public float UpButtonInterval;
        [Tooltip("Interval with which the down button should trigger when held.")] public float DownButtonInterval;

        [Header("Component References")]
        [SerializeField] private Block[] blocks;
        [SerializeField] private Material material;

        // Timers
        private float fallTimer;
        private Vector4 buttonTimers;

        // Input
        private bool left, right, up, down;

        // Movement
        [SerializeField] private Transform Target;


        private void Start()
        {
            // Initialize Timers
            fallTimer = FallCooldown;

            // Unparent Target
            Target.SetParent(null);
            //Target.hideFlags = HideFlags.HideInHierarchy;

            // Check whether start position is already invalid
            if (!IsValid())
            {
                // if not, lose the game
                GameManager.Instance.GameOver();
                Destroy(this.gameObject);
            }
        }

        private void HandleInput()
        {
            left = right = up = down = false;

            // Left Input
            if (Input.GetKey(KeyCode.LeftArrow) && buttonTimers.x <= 0.0f)
            {
                left = true;
                buttonTimers.x = LeftButtonInterval;
                buttonTimers.y = RightButtonInterval;
            }
            else if (Input.GetKeyUp(KeyCode.LeftArrow)) { buttonTimers.x = 0.0f; }

            // Right Input
            if (Input.GetKey(KeyCode.RightArrow) && buttonTimers.y <= 0.0f)
            {
                right = true;
                buttonTimers.y = RightButtonInterval;
                buttonTimers.x = LeftButtonInterval;
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow)) { buttonTimers.y = 0.0f; }

            // Up Input
            if (Input.GetKey(KeyCode.UpArrow) && buttonTimers.z <= 0.0f)
            {
                up = true;
                buttonTimers.z = UpButtonInterval;
            }
            else if (Input.GetKeyUp(KeyCode.UpArrow)) { buttonTimers.z = 0.0f; }

            // Down Input
            if (Input.GetKey(KeyCode.DownArrow) && buttonTimers.w <= 0.0f)
            {
                down = true;
                buttonTimers.w = DownButtonInterval;
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow)) { buttonTimers.w = 0.0f; }

            // Update Input Timers
            buttonTimers -= Time.deltaTime * Vector4.one;
        }

        private void Update()
        {
            if (GameManager.Instance.IsPaused) return; // don't process behaviour if game paused

            HandleInput();

            // Move Tetromino left / right
            if (left) { Move(-Vector3.right); }
            else if (right) { Move(Vector3.right); }

            // Rotate on Up
            if (up)
            {
                Rotate(90.0f);
            }

            // Fall when User presses down OR Timer up
            fallTimer -= Time.deltaTime;
            if (fallTimer <= 0.0f || down)
            {
                fallTimer = FallCooldown; // reset cooldown
                bool canMoveDown = Move(Vector3.down);

                // cannot fall further
                if (!canMoveDown) { PlaceOnGrid(); }
            }

            // Lerp to Target
            transform.position = Vector3.MoveTowards(transform.position, Target.position, Time.deltaTime * 20.0f);
            transform.rotation = Quaternion.Slerp(transform.rotation, Target.rotation, Time.deltaTime * 30.0f);
        }

        public Color GetColor()
        {
            return material.color;
        }

        public bool Move(Vector3 offset)
        {
            Target.position += offset;

            Vector3 originalPos = transform.position;
            transform.position = Target.position;

            // check whether position valid
            bool validPos = IsValid();
            if (!validPos) { Target.position -= offset; } // if the position is invalid, revert translation

            transform.position = originalPos;

            return validPos;
        }

        public bool Rotate(float angle)
        {
            Target.Rotate(Vector3.back, angle);

            Quaternion originalRot = transform.rotation;
            transform.rotation = Target.rotation;

            // check whether rotation valid
            bool validRot = IsValid();
            if (!validRot) { Target.Rotate(Vector3.back, -angle); } // if the rotation is invalid, revert it

            transform.rotation = originalRot;

            return validRot;
        }

        /// <summary>
        /// Called when the Tetromino is placed on the grid and becomes static.
        /// </summary>
        private void PlaceOnGrid()
        {
            HashSet<int> placedRows = new HashSet<int>();

            // Add all Blocks to the Grid
            foreach (Block block in blocks)
            {
                // Make the block snap to target position (avoid placing mid-lerp)
                Vector3 blockTargetPos = Target.TransformPoint(block.transform.localPosition);
                Quaternion blockTargetRot = Target.rotation * block.transform.localRotation;
                block.transform.position = blockTargetPos;
                block.transform.rotation = blockTargetRot;

                block.transform.SetParent(null, true);

                Grid.Instance.Add(block, blockTargetPos);
                block.OnPlaced();

                placedRows.Add(Mathf.RoundToInt(block.transform.position.y));
            }

            // For each row, check whether it is now full and should be cleared
            int comboMultiplier = 0;
            foreach (int row in placedRows)
            {
                comboMultiplier += Grid.Instance.ClearRowsIfFull(row);
            }

            GameManager.Instance.AddScore(comboMultiplier, this); // increase score based on rows cleared 
            

            // Delete this Tetromino (only blocks remain)
            enabled = false;
            Destroy(this.gameObject);
            Destroy(Target.gameObject);

            // Signal Spawner that it should create a new Tetromino
            Spawner.Instance.CreateNewTetromino();
        }

        /// <summary>
        /// Check whether the current Tetromino position & rotation are valid.
        /// </summary>
        /// <returns></returns>
        private bool IsValid()
        {
            // Go through all blocks
            foreach (Block block in blocks)
            {
                Vector3 blockTargetPos = Target.TransformPoint(block.transform.localPosition);
                if (!Grid.Instance.IsOnGrid(blockTargetPos)) { return false; }
                if (Grid.Instance.Get(blockTargetPos) != null) { return false; }
            }

            return true;
        }
    }
}
