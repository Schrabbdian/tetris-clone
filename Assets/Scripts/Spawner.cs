﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// This MonoBehaviour handles spawning of new Tetromino blocks. It is implemented as a Singleton for ease of access.
    /// </summary>
    public class Spawner : MonoBehaviour
    {
        public static Spawner Instance { get; private set; }

        [SerializeField] private Tetromino[] TetrominoPrefabs;

        private void Awake()
        {
            // No Spawner exists yet
            if (Instance == null)
            {
                Instance = this;
            }
            else // There is already a Spawner, destroy this
            {
                DestroyImmediate(gameObject);
            }
        }

        public void CreateNewTetromino()
        {
            int randomIdx = Random.Range(0, TetrominoPrefabs.Length);
            Tetromino tetro = Instantiate(TetrominoPrefabs[randomIdx], transform.position, Quaternion.identity);

            // Make the tetrominos fall faster the closer the Player is to the winning score
            float score = GameManager.Instance.Score;
            float winScore = GameManager.Instance.ScoreToWin;

            tetro.FallCooldown = Mathf.Lerp(tetro.FallCooldown, 0.1f, score / winScore);
        }
    }
}
