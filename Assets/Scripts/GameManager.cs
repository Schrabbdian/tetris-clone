﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

namespace Tetris
{
    /// <summary>
    /// This class is handles the high level Game Logic, such as Win / Lose Conditions, Score, etc.
    /// It is implemented as a singleton for ease of access.
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        public int ScoreToWin { get; private set; } = 8000;

        [Header("Component References")]
        [SerializeField] private Canvas UICanvas;
        [SerializeField] private TextMeshProUGUI ScoreUI;
        [SerializeField] private Animation ScoreAnimation;
        [SerializeField] private PauseMenu pauseMenu;
        [SerializeField] private StartMenu startMenu;
        [SerializeField] private RectTransform WinGUI;
        [SerializeField] private RectTransform LoseGUI;

        [Header("Prefabs")]
        [SerializeField] private Popup ScorePopupPrefab;

        private int score = 0;
        public int Score 
        { 
            get { return score; } 
            private set 
            { 
                score = value; 
                ScoreUI.text = "Score: " + Score; 
                ScoreAnimation.Play();
                if (score >= ScoreToWin) { WinGame(); } // Win game if a certain score is reached
            } 
        }

        public bool IsPaused { get; set; } = true;

        void Awake()
        {
            // If there is no Instance, set this to it
            if (Instance == null)
            {
                Instance = this;
            }
            else// If there is already one, destroy this
            {
                Destroy(this);
            }
        }

        private void Update()
        {
            if (!startMenu.isActiveAndEnabled)
            {
                // Toggle Pause if Escape pressed
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (IsPaused) { pauseMenu.Continue(); }
                    else { PauseGame(); }
                }
            }
        }

        /// <summary>
        /// Increase the Player's score.
        /// </summary>
        /// <param name="combo">How many Lines were cleared at once.</param>
        /// <param name="tetromino">The Tetromino that cause the score change.</param>
        public void AddScore(int combo, Tetromino tetromino)
        {
            if (combo <= 0) return;

            combo = Mathf.Clamp(combo, 1, 4);
            int amtToAdd = Mathf.Max(100, 400 * (combo - 1));

            // Instantiate a Popup to show the Score added
            Popup popup = Instantiate(ScorePopupPrefab, UICanvas.transform);
            popup.SetText(amtToAdd.ToString());
            popup.transform.position = Camera.main.WorldToScreenPoint(tetromino.transform.position);
            popup.SetTextColor(tetromino.GetColor());

            Score += amtToAdd;
        }

        /// <summary>
        /// Reset the current Score.
        /// </summary>
        public void ResetScore()
        {
            Score = 0;
        }

        /// <summary>
        /// Pause the Game and show the Pause Menu.
        /// </summary>
        public void PauseGame()
        {
            pauseMenu.FadeIn(); 
            IsPaused = true;

            EventSystem.current.SetSelectedGameObject(pauseMenu.transform.Find("Continue").gameObject);
        }

        /// <summary>
        /// Called when the player loses the game.
        /// </summary>
        public void GameOver()
        {
            WinGUI?.gameObject.SetActive(false);
            LoseGUI?.gameObject.SetActive(true);
            IsPaused = true;
            startMenu.FadeIn();

            EventSystem.current.SetSelectedGameObject(startMenu.transform.Find("Start").gameObject);
        }

        /// <summary>
        /// Called when the player meets a win condition.
        /// </summary>
        public void WinGame()
        {
            WinGUI?.gameObject.SetActive(true);
            LoseGUI?.gameObject.SetActive(false);
            IsPaused = true;
            startMenu.FadeIn();

            EventSystem.current.SetSelectedGameObject(startMenu.transform.Find("Start").gameObject);
        }
    }
}
