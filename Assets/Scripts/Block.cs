﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// An one-square block in the game. Used to control each individual block (that a Tetromino is made up of).
    /// </summary>
    public class Block : MonoBehaviour
    {
        [Header("Component References")]
        [SerializeField] private Animator animator;

        [Header("Effects")]
        [SerializeField] private ParticleSystem ExplodeEffectPrefab;
        [SerializeField] private GameObject EyePrefab;
        [SerializeField] private float WakeUpProbability = 0.15f;

        public bool IsAwake { get; private set;}
        private Animator eyeAnimator;

        /// <summary>
        /// Called when the block is placed on the grid.
        /// </summary>
        public void OnPlaced()
        {
            float rng = Random.Range(0.0f, 1.0f);
            
            // Wake up with the given probability
            if (rng <= WakeUpProbability)
            {
                animator?.SetBool("Alive", true);
                GameObject eyes = Instantiate(EyePrefab, transform);
                eyeAnimator = eyes.GetComponent<Animator>();
                IsAwake = true;
            }

            CheckAnxietyInColumn();
        }

        public void SetAnxious(bool isAnxious)
        {
            eyeAnimator?.SetBool("Anxious", isAnxious);
        }

        /// <summary>
        /// Called to initiate the Explode animation.
        /// </summary>
        public void Explode()
        {
            animator?.SetTrigger("Explode");
        }

        /// <summary>
        /// Actually destroys the block and shifts the clumn above it downwards in the grid. Automatically called from an Animation Event.
        /// </summary>
        public void DestroyBlock()
        {
            ParticleSystem effect = Instantiate(ExplodeEffectPrefab, transform.position, Quaternion.identity);
            ParticleSystemRenderer renderer = effect.GetComponent<ParticleSystemRenderer>();

            if (renderer) { renderer.material = GetComponent<MeshRenderer>().material; }

            // Shift grid column downwards
            int x = Mathf.RoundToInt(transform.position.x);
            int y = Mathf.RoundToInt(transform.position.y);

            Grid.Instance.RemoveAt(transform.position);
            Grid.Instance.ShiftColumnDownwards(x, y + 1);

            CheckAnxietyInColumn();

            Destroy(gameObject);
        }

        private void CheckAnxietyInColumn()
        {
            // Go through blocks below
            int x = Mathf.RoundToInt(transform.position.x);
            int noPiledBlocks = 0;

            for (int j = Grid.Instance.Height - 1; j >= 0; j--)
            {
                Block b = Grid.Instance.Get(new Vector2(x, j));
                if (b == null) { noPiledBlocks = 0; }
                else
                {
                    noPiledBlocks++;
                    if (!b.IsAwake) continue;

                    // Set blocks to be anxious when there are more than 3 piled on top
                    b.SetAnxious(noPiledBlocks > 4);
                }
            }
        }
    }
}
