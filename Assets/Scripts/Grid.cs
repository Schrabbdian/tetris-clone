﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tetris
{
    /// <summary>
    /// The grid that acts as the play field for Tetris. Provides functions for checking whether a position is valid,
    /// for referencing all Tetris blocks in a row, etc. 
    /// It is implemented as a Singleton for ease of access.
    /// </summary>
    public class Grid : MonoBehaviour
    {
        // Singleton
        public static Grid Instance { get; private set; }

        public int Width { get; private set; } = 10;
        public int Height { get; private set; } = 20;

        private Block[,] grid; // grid of all placed blocks

        private void Awake()
        {
            // No Grid exists yet
            if (Instance == null)
            {
                grid = new Block[Width, Height];
                Instance = this;
            }
            else // Another Grid already exists
            {
                DestroyImmediate(gameObject);// destroy this one
            }
        }


        /// <summary>
        /// Determine whether a position is on the Grid or not.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public bool IsOnGrid(Vector2 position)
        {
            int x = Mathf.RoundToInt(position.x);
            int y = Mathf.RoundToInt(position.y);

            return x >= 0 && x < Width && y >= 0 && y < Height;
        }


        /// <summary>
        /// Add a Block to the Grid.
        /// </summary>
        /// <param name="block"></param>
        /// <param name="position"></param>
        public void Add(Block block, Vector2 position)
        {
            int x = Mathf.RoundToInt(position.x);
            int y = Mathf.RoundToInt(position.y);

            grid[x, y] = block;
        }

        /// <summary>
        /// Clear the entire grid.
        /// </summary>
        public void Clear()
        {
            for (int i = Height; i >= 0; i--)
            {
                ClearRow(i);
            }
        }

        /// <summary>
        /// Check whether the given rows are full and if so, clear them.
        /// </summary>
        /// <param name="rows"></param>
        /// <returns>Number of rows cleared.</returns>
        public int ClearRowsIfFull(params int[] rows)
        {
            int noCleared = 0;

            foreach (int r in rows)
            {
                // Check whether the row is full and clear it if so
                bool isRowFull = IsRowFull(r);
                if (isRowFull)
                {
                    ClearRow(r);
                    noCleared++;
                }
            }
            return noCleared;
        }

        /// <summary>
        /// Check whether a row of the grid is full or not.
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool IsRowFull(int y)
        {
            if (y < 0 || y > Height - 1) return false;

            // Go through the row and check whether each cell is filled
            for (int x = 0; x < Width; x++)
            {
                if (grid[x, y] == null) return false;
            }

            return true;
        }

        private void ClearRow(int y)
        {
            if (y < 0 || y > Height - 1) return;

            // Go through the row and Destroy each Object
            for (int x = 0; x < Width; x++)
            {
                Block b = grid[x, y];
                if (b == null) continue;

                // Cause the block to explode and shift its column downwards when done
                b.Explode();
            }
        }


        /// <summary>
        /// Get the Block at the given position on the Grid.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public Block Get(Vector2 position)
        {
            int x = Mathf.Clamp(Mathf.RoundToInt(position.x), 0, Width -1);
            int y = Mathf.Clamp(Mathf.RoundToInt(position.y), 0, Height -1);

            return grid[x, y];
        }

        /// <summary>
        /// Remove the Block at the specified position from the grid.
        /// </summary>
        /// <param name="position"></param>
        public void RemoveAt(Vector2 position)
        {
            int x = Mathf.Clamp(Mathf.RoundToInt(position.x), 0, Width - 1);
            int y = Mathf.Clamp(Mathf.RoundToInt(position.y), 0, Height - 1);

            grid[x, y] = null;
        }

        /// <summary>
        /// Shift all blocks above the specified row and in the specified column downwards by one.
        /// </summary>
        /// <param name="col">Column to shift.</param>
        /// <param name="row">Height above which cells should be shifted downwards.</param>
        public void ShiftColumnDownwards(int col, int row)
        {
            for (int y = row; y < Height; y++)
            {
                // Shift each Block down by one cell
                Block b = grid[col, y];
                if (b == null) continue;

                grid[col, y - 1] = b;
                grid[col, y] = null;
                b.transform.position -= Vector3.up;
            }
        }
    }
}
